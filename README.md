# Server for AeroSim
## usage v0.2 without Docker
- init server/.env file with: 
    - HTTP_PORT
    - UDP_PORT
    - UDP_HOST

On PowerShell:
- cd .\server\; npm i; npm start
On Shell:
- cd .\server\ && npm i && npm start

It run server

### For development usage
Use 'npm run dev' (nodemon)