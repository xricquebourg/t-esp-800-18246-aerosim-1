const dgram = require("dgram");
const server = dgram.createSocket("udp4");


server.on("message", (msg, senderInfo) => {
    console.log(`Messages received: ${msg}`);
    // Respond to the sender
    server.send("Pong !", senderInfo.port, senderInfo.address, () => {
        // Log that we've respond
        console.log(`Pong sent to ${senderInfo.address}:${senderInfo.port}`);
    });
});

module.exports = server;