const express = require("express");
const router = express.Router();
// const UserRouter = require("./user/user.router.js");
// const ping = require("./util/ping.js");

// Router.use("/users", UserRouter);

router.get(
    "/ping",
    (request, response) => {
        return response.status(200).json({ data: "Pong !" });
    }
);

module.exports = router;