require("dotenv").config();

// Create config to avoid "process.env" usage in app
const config = {
    udp: {
        port: process.env.UDP_PORT || 5500,
        host: process.env.UDP_HOST || "localhost"
    },
    http: {
        port: process.env.HTTP_PORT || 3000,
    }
};

module.exports = config;