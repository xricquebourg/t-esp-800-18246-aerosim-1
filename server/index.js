// Import
const config = require("./src/config/config.js");
const udp = require("./src/api/udp.server");
const http = require("./src/config/http.server");

// Start HTTP server
http.listen(config.http.port, () => {
    // Set up logger for traceability
    console.info(`HTTP server running on port ${config.http.port}`);
});


// Start UDP server
const startUdpServer = () => {
    udp.on("listening", () => {
        const address = udp.address();
        console.log(`UDP server listening on ${address.address}:${address.port}`);
    });

    udp.bind({
        address: config.udp.host,
        port: config.udp.port,
        exclusive: true
    });
};

// Handle UDP erros
udp.on("error", (err) => {
    console.log(`Server error:\n${err.stack}`);
    udp.close();
    startUdpServer();
});

startUdpServer();