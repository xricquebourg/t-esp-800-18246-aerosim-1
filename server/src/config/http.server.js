const express = require("express");
const cors = require("cors");
const router = require("../api/router.js");

const server = express();

// const server = http.Server(server);

// const init = async () => {


server.use(cors());
server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(router);
// };

module.exports = server;